#Beginner programming courses

**Platform** : Any platform is fine, linux recommended.

###BEG01 Programming Fundamentals using python

**Duration** : 3 weeks(5 days each week)

**Topics** : types, expressions, conditions, loops, collections, lists

**Description** : If you wish to learn to program, familiar with some programming language but didn't write much code, this is the batch for you. We will slowly learn basics, give you assignments daily and all the programs you write will be reviewed the next day in the class. 

###BEG02 Object oriented programming fundamentals using python

**Duration** : 3 weeks(5 days per week)

**Topics** : objects, classes, contructors, methods, properties, polymorphism, composition, inheritance.

**Prerequisites** : BEG01

**Description** : This slow paced batch will teach you OOP, which is a difficult topic. You will be given simple assignments every day and reviewed next day in the class.

###BEG03 Functional programming fundamentals using python

**Duration** : 3 weeks

**Topics** : functions, closures, lambda expressions, higher order functions, generators

**Prerequisites** : BEG01

**Description** : This would be a gentle introduction to functional programming. Only those topics which are relevant to mainstream imperative languages like C#, javascript, C++, Java are discussed.

##Other programming languages

*If you do not prefer python, we **might** offer these courses in other languages*

###Ruby

####BEG04 Programming Fundamentals using ruby

####BEG05 Object oriented programming fundamentals using ruby

####BEG06 Functional programming fundamentals using ruby

###Scala

####BEG07 Programming Fundamentals using scala

####BEG08 Object oriented programming fundamentals using scala

####BEG09 Functional programming fundamentals using scala

### FSharp

*Please note I **do not** recommend FSharp for these courses.*

####BEG10 Programming Fundamentals using FSharp

####BEG11 Object oriented programming fundamentals using FSharp

####BEG12 Functional programming fundamentals using FSharp

