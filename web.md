#Web Development courses

**Platform** : Mac or linux highly recommended if not mentioned.

###WEB06 HTML5

**Duration** : 2 Weeks

**Prerequisites** : WEB02

**Topics** : web storage, web Workers, web sockets, history API

###WEB07 Asynchronous javascript

**Duration** : 1 Week

**Prerequisites** : WEB02

**Topics** : callbacks, promises, deferred, ES6 generators, iced coffeescript

###WEB08 KnockoutJS

**Duration** : 2 weeks

**Platform** : Windows recommended

**Prerequisites** : WEB02

**Recommended** : WEB03

**Topics** : bindings, control flow, forms, templates, custom bindings, validation

###WEB09 Backbonejs

**Duration** : 3 weeks

**Prerequisites** : WEB02, WEB03

**Topics** : models, views, collections, events, routing, ajax, templating

###WEB10 AngularJS

**Duration** : 3 weeks

**Prerequisites** : WEB02, WEB03  

**Topics** : controllers, directives, templates, filters, bindings, ajax, dependency injection, routing, events, REST, services, resources, modules, tranclusion, unit testing, integration testing

###WEB11 EmberJS

**Duration** : 3 weeks

**Prerequisites** : WEB02, WEB03

**Topics** : templates, handlebars, routing, components, models, views, enumerables, testing

###Web12 Mobile Web

**Duration** : 3 weeks

**Prerequisites** : WEB02, WEB03

**Topics** : jquery mobile, phonegap

###Web13 Coffeescript

**Duration** : 2 Weeks

**Prerequisites** : WEB03

**Topics** : functions, classes, inheritance, functional programming, underscorejs

###Web14 Typescript

**Duration** : 2 Weeks

**Platform** : Windows

**Prerequisites** : WEB02

**Topics** : types, functions, classes, interfaces, inheritance, generics, modules


###Web15 Ruby on rails

**Duration** : 4 weeks

**Prerequisites** : Ruby

**Topics** : activerecord, migrations, validations, associations, queries, views, layouts, partials, helpers, controllers, routing, ajax, asset pipeline, unit testing, integration testing

###Web16 ASP.NET MVC

**Duration** : 4 weeks

**Prerequisites** : NET01, NET02, NET03, WEB05

**Recommended** : NET04, NET05

**Topics** :  models, validations, views, razor syntax, layout, partials, helpers, controllers, routing, web api, ajax, session handling, unit testing, integration testing, dependency injection and tools.

###Web18 NodeJS

**Duration** : 3 weeks

**Prerequisites** : WEB05, web06, WEB08

**Topics** : nodejs, socket.io, express

###Web19 Play Framework

**Duration** : 3 weeks

**Prerequisites** : WEB05 and fundamentals, scala

**Topics** : asynchronous HTTP, REST, web sockets, view templates, persistence

