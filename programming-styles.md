#Programming styles

###PRS01 Object oriented Programming using scala and ruby

**Duration** : 6 weeks 

**Topics** : abscraction, encapsulation, polymorphism, composition, inheritance, mixins, traits,SOLID principles(SRP, LSP, OCP etc), design patterns(composite, decorator,prototype, observer, strategy, command etc)

**Description** : 

###PRS02 Functional Programming using haskell and clojure 

**Duration** : 6 weeks

**Description** : You could treat this batch either as exploration of functional programming or just programming concpets. I am not providing topics for this as it will be difficult.

###PRS03 Generic Programming using C++ and D

**Duration** : 6 weeks

**Topics** : templates, concepts, typing

**Description** : Most if not all of the generic programming techniques and idioms will be discussed with examples from standard template library, boost and standard D library.

