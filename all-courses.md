#All courses

There have been some querues on beginner courses in the last few months. Unfortunately I have not been able to take classes for beginners. It's really difficult to take beginner classes. Most expect to start with C#, C++ like languages, which are huge and complex. There is no way you could learn the whole language in one go. Taking beginner C# course forces me to cover all the topics in one go, which is overwhelming for any beginner. 

The other problem I have almost always faced when taking a batch is I somehow seem to get beginners as well as experienced programmers in the same batch, as it happened with my recent data structures class. Knowing what you need to attend a course is important.

Third problem students face is not knowing what to learn next. Most need a particular topic in their professional work and just attend relevant course. Never knowing how everything works, not knowing prerequisites. I too take courses as students request in a rather haphazard fashion rather than having some order.

I have organised most of the courses we take which will give you indication what you need to learn, and what you need for each batch. If you can work hard, show interest in learning rather than bling and are disciplined I am sure you will be a far better programmer with the help of the following courses.

I must again remind you that these are the best times for any programmer, and perhaps the best time to learn new skills, as lot's of enthusiasm around you will keep you interested.

Let me provide explainations for these batches.

##Beginners

If you are a beginner, hardly programmed before I recommend you learn the basics at a slower pace, watching me write good amount of code, writing code yourself and getting reviewd by me. I have three programming courses for beginners using python. You can opt for ruby instead. You could also use scala(if you intend to work on JVM) or F#(if you intend to work on .NET). I am not offering any complete beginner C#, C++ or Java courses. You need to concentrate on learning the concepts well, not the feature set or syntax provided by your programming language. 

####Beginner batches

+ (BEG01) Programming Fundamentals using python
+ (BEG02) Object oriented programming fundamentals using python
+ (BEG03) Functional programming fundamentals using python

Web is important, it's huge and can be overwhelming. Following web fundamentals will help you start well. 

####Web development fundamentals

* (WEB01) HTML and CSS
* (WEB02) Javascript 
* (WEB03) jQuery
* (WEB04) Unit Testing & Tools
* (WEB05) Web server side fundamentals using Sinatra

##Beginner-Intermediate programmers

I would strongly recommend you better your fundamentals. Strong fundamentals allow you to learn faster. Most programmers spend far more time learning newer stuff than needed as fundamentals are weak. These fundamentals are short enough, concentrate on the concepts rahter than esoteric programming language features. Programming language I choose is again irrelevant, I will use python or ruby if you instead. 

*yes, these are fun batches. ;-)*

####Fundamentals

* (FUN01) Tools
* (FUN02) Basic Computer Hardware
* (FUN03) Operating Systems
* (FUN04) Multithreading
* (FUN05) Network Programming
* (FUN06) Data Structures

.net developers need to know the language well, need to deal with concurrency, asynchrony and most developers work on web applications. Following courses will teach you exactly these topics.

####.NET

* (NET01) C# Basics
* (NET02) C# OOP Basics
* (NET03) C# OOP Intermediate
* (NET04) C# Functional Programming
* (NET05) LINQ

C++ programmers need to know their machine, learn the whole language, and most work on system software. Following courses will deal with these courses.

####C++

* (CPP01) Modern C++
* (CPP02) Object oriented programming in C++
* (CPP03) Generic programming in C++
* (CPP04) Functional programming using C++
* (CPP05) Error handling in C++
* (CPP06) Standard C++ library 

####Low level C++

* (CPP07) Programming in C
* (CPP08) Assembly Language
* (CPP09) Memory Management in C++

Web is huge, there are lot's of things to learn. There are many frameworks, nmany options. Pick and choose the ones you are interested in.

* (WEB06) HTML5
* (WEB07) Asynchronous javascript
* (WEB12) Mobile web

####Client side MV* Frameworks

* (WEB08) KnockoutJS
* (WEB09) Backbonejs
* (WEB10) AngularJS
* (WEB11) EmberJS 

####Programming languages

* (WEB13) Coffeescript
* (WEB14) Typescript

####Server side web development

* (WEB15) Ruby on rails
* (WEB16) ASP.NET MVC
* (WEB18) NodeJS
* (WEB19) Play Framework

##Advanced programmers

Knowing multiple programming languages will equip you with more ways of solving problems. You will know more set of abstractions. Most features included in C# and C++ recent versions are from the classical programming languages. I personally believe haskell, lisp and erlang teach you almost every abastraction you need to be aware of as a programmer in today's world.  

Three modern languages heavily influenced by the three programming languages are getting a lot of attention. Scala, clojure and elixir. These languages not only are heavily inspired, hence borrow a lot of features from these three programming languages, they also add features needed for today's world making it possible to use these languages in today's world. scala is a modern take on statically typed functional programming language, clojure is modern lisp on JVM with excellent support for concurrency and elixir is modern erlang. Folowing courses will teach you these three modern programming languages. 

####Classical programming languages

* (CPL01) Haskell
* (CPL02) Lisp
* (CPL03) Erlang

Many of the times we lose the sight of forest for the trees. Sometimes we get obsessed with programming languages and do not understand concepts well. Or how they work with each other to make things possible. Following three courses will teach you three most important paradigms today functional, object oriented and generic programming from the programming paradigm view instead of language features view. This should be considered an advanced course and you need to know most languages to be able to attend these courses. Consider these as our most advanced courses.

####Modern programming languages

* (MPL01) scala
* (MPL02) clojure
* (MPL03) elixir

####Advanced C#

* (NET06) .NET platform
* (NET07) C# Concurrency
* (NET08) Functional programming with F#

####Advanced C++

* (CPP10) Metaprogramming in C++
* (CPP11) Concurrency with C++
* (CPP12) Linux programming in C++
* (CPP13) Network programming in C++

