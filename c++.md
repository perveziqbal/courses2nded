#C++ courses   
 
**Platform** : Linux highly recommended, mac can be used for most courses. I don't recommend Windows for these batches, anyway you can use it for most of the batches.

##C++ normal courses

###CPP01 Modern C++

**Duration** : 3 weeks

**Prerequsites** : BEG01

**Topics** : standard C++ library, collections, iterators, algorithms, function objects, binders, lambda expressions, initializer lists, type inference, constexpr, inline functions, operator overloading, range based for loop, nullptr, enums, decltype, template aliases, unit testing

###CPP02 Object oriented programming in C++

**Duration** : 3 weeks

**Prerequisites** : BEG02, CPP01

**Topics** : identity, state, behaviour, inheritance, multiple inheritance, mixins, smart pointers

###CPP03 Generic programming in C++

**Duration** : 3 weeks

**Prerequisites** : CPP02 

**Topics** : concepts, type traits, policy based design, SFINAE

###CPP04 Functional programming using C++

**Duration** : 2 weeks

**Prerequisites** : BEG03, CPP02 

**Topics** : values, collections, iterators, ranges, functions, lambda expressions, binders, higher order functions 

###CPP05 Error handling in C++

**Duration** : 2 weeks

**Prerequisites** : CPP02

**Topics** : contracts, exceptions safety, RAII

###CPP06 Standard C++ library 

**Duration** : 3 weeks

**Prerequisites** : CPP03, CPP04, CPP05

**Topics** : algorithms, collections, iterators, function objects, adapters, time and space compexity, exception safety

##Low level C++ courses

###CPP07 Programming in C

**Duration** : 2 Weeks

**Prerequisites** : FUN01, CPP01

**Topics** : functions, pointers, arrays, structs

###CPP08 Assembly Language

**Duration** : 1 Week

**Recommended** : CPP08

**Topics** : Machine instructions for arithmetic, logical and relational operations, conditions, loops, functions, global variables, local variables and arguments

###CPP09 Memory Management in C++

**Duration** : 2 Weeks 

**Prerequisites** : CPP07

**Topics** : RAII, big three, smart pointers, overloading new and delete, move semantics

**Description** : This is another low level batch discussing how we manage memory in C++.


##C++ advanced courses

###CPP10 Metaprogramming in C++

**Duration** : 3 weeks

**Prerequisites** : CPP06

**Topics** : compile time evaluation, meta functions

###CPP11 Concurrency with C++

**Duration** : 4 weeks

**Prerequisites** : FUN04, CPP04

**Topics** : threads, synchronization, C++ memory model, atomic, thread local storage, lock free data structures, thread pools, async, promises, futures 

###CPP12 Linux programming in C++

**Duration** : 3 weeks

**Prerequisites** : FUN04, CPP04

**Recommended** : CPP11

**Topics** : processes, shared memory, IPC, semaphores, messaging, virtual memory, memory management

###CPP13 Network programming in C++

**Duration** : 3 weeks

**Prerequisites** : FUN05

**Recommended** : CPP12

**Topics** : TCP, UDP, IP, asynchronous IO

