#Fundamentals

**Prerequisite for all courses** : You should be good atleast in one programming language C#, Java, C++ preferrable.

**Platform** : Linux highly recommended, mac too can be used. You can use vmware or virtualbox on windows for both operating systems.

**Programming language** : python if not mentioned. I believe programming language does not matter in these courses, so I might not provide any other language as an option, except perhaps ruby.

###FUN01 Tools

**Duration** : 2 weeks

**Topics** : command line, git, emacs/vi, vagrant

###FUN02 Basic Computer Hardware

**Duration** : 1 week

**Topics** : simple x86 instructions for arithmetic, logical operators and relation operators, function calls, interrupts, machine boot process, interrupt service routines, segments(code, data, stack). Not an independent batch, prerequisite to operating systems, multithreading courses

###FUN03 Operating Systems

**Duration** : 2 Weeks

**Prerequisites** : FUN01, FUN02

**Topics** : Processes(fork, exec, wait, pipe) and files(open, close, read, write, lseek) system calls.

###FUN04 Multithreading

**Duration** : 2 Weeks

**Prerequisites** : FUN02

**Topics** : threads, locks, condition variables.

###FUN05 Network Programming

**Duration** : 2 weeks

**Prerequisites** : FUN03

**Recommended** : FUN04

**Topics** : TCP, UDP sockets, non blocking IO, multiplexed IO.

###FUN06 Data Structures

**Duration** : 3 weeks

**Topics** : searching, sorting, linked lists, binary search trees, hash tables

**Description** : This will be an introductory course discussing only those topics which are required for any working professional. Seperate courses will be offered for those who are interested in the topic.


