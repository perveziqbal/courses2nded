#Web Development Fundamentals

**Prerequisites for all courses** : BEG01, BEG02, BEG03 required, FUN02, FUN01, FUN02, FUN03, FUN05 and FUN06 recommended

**Platform** : Linux preferred, Mac or Windows can be used. 

###Web01 HTML and CSS

**Duration** : 2 weeks

**Topics** : forms, tables, css, inheritance, selecctors, layout, twitter bootstrap, sass

###WEB02 Javascript 

**Duration** : 2 Weeks

**Topics** : types, expressions, functions, objects, arrays, prototypes, modules, inheritance

**Description** : Thorough discussion of all topics relevant to javascript developers. This can be treated as an intermediate course.

###WEB03 jQuery

**Duration** : 2 Weeks

**Prerequisites** : WEB02

**Topics** : DOM, events, ajax, promises and deferreds, templates

###WEB04 Unit Testing & Tools

**Duration** : 2 Weeks

**Prerequisites** : WEB03

**Topics** : jasmine, sinon, amplify, selenium, grunt, bower, yeoman

###WEB05 Web server side fundamentals using Sinatra

**Duration** : 3 Weeks

**Topics** : HTTP, REST, session Handling, templates, partials, layouts, routing, bundling, minification, slim, sass, unit testing, integration testing, assets, tools



