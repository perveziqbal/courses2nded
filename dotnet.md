#.NET batches

###NET01 C# Basics

**Duration** : 1 week

**Prerequisites** : BEG01

**Topics**: expressions, statements, functions, arrays, collections,  object initializers, collection initializers

###NET02 C# OOP Basics

**Duration** : 3 weeks

**Prerequisites** : BEG02, NET01

**Topics** : classes, objects, constructor, properties, methods, inheritance

###NET03 C# OOP Intermediate

**Duration** : 3 weeks

**Prerequisites** : NET02

**Topics**: interfaces, generics, covariance and contravariance, events, iterator blocks, exception handling

###NET04 C# Functional Programming

**Duration** : BEG03, NET03

**Prerequisites** : C# OOP Intermediate course

**Topics**: delegates, closures, lambda expressions, extension methods, type inference, anonymous classes, named and default arguments, higher order functions

###NET05 LINQ

**Duration** : 3 weeks

**Prerequisites** : NET04

**Topics** : LINQ operators, LINQ to objects, LINQ to XML

###NET06 .NET platform

**Duration** : NET03

**Prerequisites** : C# OOP Intermediate

**Topics**: garbage collection, object model, assemblies, MSIL, debugging, performance

###NET07 C# Concurrency

**Duration** : 4 weeks

**Prerequisites** : FUN04, NET03

**Topics** : C# memory model, threads, threadpools, locks, tasks, TPL, PLINQ, async and await

###NET08 Functional programming with F#

**Duration** : 4 weeks

**Prerequisites** : FUN04, NET03

**Recommended** : NET04

**Topics**: recursion, functions, immutability, records, higher order functions, pattern matching, workflows, sequences, async, type providers 

