#Programming languages

**Prerequistes for all courses** : All BEG and FUN courses and one of CPP normal courses or NET normal courses

##Classical Programming languages

###CPL01 Haskell

**Duration** : 4-6 weeks

**Topics** : typing, functional programming, monads, lazy evaluation

###CPL02 Lisp

**Duration** : 4-6 weeks

**Topics** : lists, functions, macros 

###CPL03 Erlang

**Duration** : 4 weeks

**Topics** : functional programming, concurrency using actors
 

##Modern programming languages

###MPL01 scala

**Duration** : 6 weeks

**Recommended** : CPL001

**Topics** : Object oriented programming, functional programming, concurrency


###MPL02 clojure

**Duration** : 4-6 weeks

**REcommended** : CPL02

**Topics** : functional programming, macros, concurrency


###MPL03 elixir

**Duration** : 4 weeks

**Recommended** : CPL03

**Topics** :  functional programming, concurrency

